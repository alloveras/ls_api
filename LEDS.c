/*
================================================================================
 * Nom: DRIVER LEDS
 * Fitxer: LEDS.c
 * Funcionalitat: Controla els HeartBeat LEDS.
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * �ltima Revisi�: 26/01/2015
================================================================================
*/

//Include del fitxer de cap�aleres
#include "LEDS.h"

/*
===============================================================================
                              CONSTANTS PRIVADES
===============================================================================
*/

#define LED_ON 0
#define LED_OFF 1
#define SET_LED_PIN_DIRECTION() TRISDbits.TRISD4 = 0
#define SET_LED_VALUE(nStatus) LATDbits.LATD4 = nStatus

/*
================================================================================
                               VARIABLES PRIVADES
================================================================================
*/
static unsigned char nEstat;
static unsigned char nTimer;
static unsigned char nIntensity;

//Pre: Cap
//Post: Inicialitza el TAD LEDS posant el port HeartBeat de sortida i posant
//l'estat de la m�quina d'estats del motor al estat 0.
void LEDS_Init(void){

    //Inicialitzem la m�quina d'estats a l'estat 0
    nEstat = 0;

    //Agafem Timer
    nTimer = T_Timers_GetTimer();

    if(nTimer != -1){

        //Fixem la intensitat del LED per defecte a apagat
        nIntensity = 1;

        //Configurem el port del HearBeat LED de sortida
        SET_LED_PIN_DIRECTION();

        //Per defecte deixem el LED apagat
        SET_LED_VALUE(LED_OFF);

    }

}

//Pre: nIntensity ha de valdre entre 0 i 10
//Post: Fixa la lluminositat dels LEDS de HeartBeat
void LEDS_SetIntensity(unsigned char nValue){
    if(nValue >= 0 && nValue <= 10){
        nIntensity= nValue;
    }
}

//Pre : Cap
//Post: Cap
void LEDS_StatusMachine(void){
    switch(nEstat){
        case 0:
            if(nIntensity > 0) SET_LED_VALUE(LED_ON);
            if(T_Timers_GetValue(nTimer) >= nIntensity){
                nEstat++;
                T_Timers_ResetTimer(nTimer);
            }
            break;
        case 1:
            SET_LED_VALUE(LED_OFF);
            if(T_Timers_GetValue(nTimer) >= 10 - nIntensity){
                nEstat = 0;
                T_Timers_ResetTimer(nTimer);
            }
            break;  
    }
}
/*
================================================================================
 * Nom: TAD HEARTBEAT
 * Fitxer: T_HeartBeat.h
 * Funcionalitat: Generar l'efecte de pols card�ac als LEDS del LSMaker
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * �ltima Revisi�: 26/01/2015
================================================================================
*/

#ifndef _T_HEARTBEAT_H_
#define T_HEARTBEAT_H_

//Includes TADs necessaris
#include "T_Timers.h"

//Includes drivers necessaris
#include "LEDS.h"

/*
================================================================================
                             FUNCIONS I PROCEDIMENTS
================================================================================
*/

//Pre: Cap
//Post: Inicialitza el TAD HeartBeat amb les configuracions que permeten fer
//canviar els LEDS d'intensitat de manera creixent i decreixent amb per�ode per
//defecte de 120 ms
void T_HeartBeat_Init(void);

//Pre: Cap
//Post: Cap
void T_HeartBeat_StatusMachine(void);

//Pre: nPeriod ha de valdre entre 0 i 32768
//Post: Fixa el per�ode de canvi d'intensitat al valor rebut per par�metre.
void T_HeartBeat_SetPeriod(unsigned int nPeriod);

#endif
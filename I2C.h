/*
================================================================================
 * Nom: DRIVER I2C
 * Fitxer: I2C.h
 * Funcionalitat: Controla l'acc�s als busos I2C del robot
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * �ltima Revisi�: 24/01/2015
================================================================================
*/

//Includes del MCU
#include <xc.h>

//Include drivers necess�ris
#include "T_Timers.h"

//Pre: El TAD Timers ha d'estar inicialitzat
//Post: Configura els registres de l'MCU per activar el bus I2C1 funcionant en
//mode m�ster i amb una velocitat de 400Kbps
void I2C1_Init(void);

//Pre: S'ha d'haver comprovat que la cua de sortida del bus I2C t� espai
//t� espai suficient per albergar un paquet I2C m�s.
//Post: Encua el paquet I2C a la cua de sortida per ser enviar el m�s aviat
//possible.
void I2C1_WriteByte(char cDeviceAddr,char cRegisterAddr,char byte);

//Pre: S'ha d'haver comprovat pr�viament que no s'est� rebent cap altre dada a
//trav�s del bus I2C.
//Post: Encua la petici� de lectura d'un registre d'un dispositiu remot a la 
//llista de tasques del driver. Mentre aquesta no es realitzi, es descartar� 
//qualsevol altre petici� de lectura. D'aquesta manera, quan la lectura acabi,
//ja sigui satisfact�riament o no, el bus I2C tornar� a estar llest per rebre
//una petici� de lectura.
void I2C1_ReadByte(char cDeviceAddr, char cRegisterAddr, char *byte);

//Pre : Cap
//Post: Retornar� 1 (CERT) si hi ha alguna petici� de lectura pendent per
//processar-se. En qualsevol altre cas, retornar� 0 (FALS).
unsigned char I2C1_isReceiving(void);

//Pre: Cap
//Post: Retornar� 1 (CERT) si la cua de sortida del bus I2C cont� espai per
//tants paquets I2C com nItems s'hagin especificat. Altrament, retornar� 0 que
//equival a (FALS).
char I2C1_OutputQueueHasSpace(unsigned char nItems);

//Pre: Cap
//Post: Cap
void I2C1_StatusMachine(void);
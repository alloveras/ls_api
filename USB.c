/*
================================================================================
 * Nom: USB
 * Fitxer: USB.c
 * Funcionalitat: Driver per poder enviar i rebre dades a trav�s del bus USB.
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * �ltima Revisi�: 27/01/2015
================================================================================
*/

//Include de les cap�aleres
#include "USB.h"

/*
================================================================================
                              CONSTANTS PRIVADES
================================================================================
*/

#define MAX_QUEUE 8


/*
================================================================================
                               TIPUS PRIVATS
================================================================================
*/
typedef struct{
    unsigned char nStart;
    unsigned char nEnd;
    unsigned char nSize;
    char aQueue[MAX_QUEUE];
}USB_Queue;


/*
================================================================================
                            VARIABLES PRIVADES
================================================================================
*/

static USB_Queue RXQueue;
static USB_Queue TXQueue;
static unsigned char nEstat = 0;

/*
================================================================================
                      PROCEDIMENTS I FUNCIONS PRIVADES
================================================================================
*/

static char  Pop(USB_Queue *cua){
    char cReturn = ' ';
    if(cua->nSize > 0){
        cReturn = cua->aQueue[cua->nStart];
        cua->nStart++;
        if(cua->nStart == MAX_QUEUE) cua->nStart = 0;
        cua->nSize--;
    }
    return cReturn;
}


static void Push(USB_Queue *cua,char byte){
    if(cua->nSize < MAX_QUEUE){
        cua->aQueue[cua->nEnd] = byte;
        cua->nEnd++;
        if(cua->nEnd == MAX_QUEUE) cua->nEnd = 0;
        cua->nSize++;
    }else{
        Pop(cua);
        cua->aQueue[cua->nEnd] = byte;
        cua->nEnd++;
        if(cua->nEnd == MAX_QUEUE) cua->nEnd = 0;
        cua->nSize++;
    }
}

void __attribute__((interrupt, no_auto_psv)) _U1RXInterrupt(void) {
    char data;
    IFS0bits.U1RXIF = 0;
    if (U1STAbits.OERR == 1) U1STAbits.OERR = 0;
    data = U1RXREG;
    Push(&TXQueue,data);
    Push(&RXQueue,data);    //Encuem a la cua de recepci�
}

/*
================================================================================
                      PROCEDIMENTS I FUNCIONS P�BLICS
================================================================================
*/

//Pre: Cap
//Post: Inicialitza el bus USB amb un baudrate de 9600, transmissi� de 8 bits
//sense paritat i amb un stop-bit.
void USB_Init(void){

    TXQueue.nEnd = 0;
    TXQueue.nStart = 0;
    TXQueue.nSize = 0;

    RXQueue.nEnd = 0;
    RXQueue.nSize = 0;
    RXQueue.nStart = 0;

    U1MODE = 0x8000;
    U1STA = 0x2400;
    U1BRG = 103;
    IEC0bits.U1RXIE = 1;
    IPC2bits.U1RXIP = 6;

    nEstat = 0;
}

//Pre: Cap
//Post: Cap
void USB_StatusMachine(void){

    switch(nEstat){
        case 0:
            if(TXQueue.nSize > 0){
                U1TXREG = Pop(&TXQueue);
                nEstat++;
            }
            break;
        case 1:
            if(U1STAbits.TRMT == 1){
                nEstat = 0;
            }
            break;
    }


}

//Pre: S'ha d'haver comprovat pr�viament que la cua de sortida no estigui plena.
//Post: Encua el byte rebut per par�metre a la cua de sortida del bus USB per
//tal d'enviar-lo quan sigui possible.
void USB_WriteByte(char byte){
    Push(&TXQueue,byte);
}

//Pre: S'ha d'haver comprovat que hi ha almenys un byte a la cua d'entrada del
//bus USB.
//Post: Retorna el primer byte encuat a la cua d'entrda del bus USB. S'ha de
//tenir en compte que la lectura �s DESTRUCTIVA.
char USB_ReadByte(void){
    char nReturn = ' ';
    if(RXQueue.nSize > 0){
        nReturn = Pop(&RXQueue);
    }
    return nReturn;
}

//Pre: Cap
//Post: Retorna 1 (CERT) si la cua de sortida del bus USB no t� espai disponible
//per un nou byte a enviar. Altrament, aquesta funci� retornar� 0 (FALS).
unsigned char USB_OutputQueueFull(void){
    return TXQueue.nSize == MAX_QUEUE;
}

//Pre: Cap
//Post: Retorna 1 (CERT) si la cua d'entrada del bus USB te al menys 1 byte
//pendent de ser llegit. En qualsevol altre cas, aquesta funci� retornar� 0
//(FALS).
unsigned char USB_InputQueueEmpty(void){
    return RXQueue.nSize == 0;
}

/*
================================================================================
 * Nom: TAD HEARTBEAT
 * Fitxer: T_HeartBeat.h
 * Funcionalitat: Generar l'efecte de pols card�ac als LEDS del LSMaker
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * �ltima Revisi�: 26/01/2015
================================================================================
*/

//Include del fitxer de cap�aleres
#include "T_HeartBeat.h"


/*
================================================================================
                               VARIABLES PRIVADES
================================================================================
*/

static unsigned char nEstat = 0;    //Indicador d'estat de la m�quina d'estats
static unsigned int nPeriod = 2000; //Per�ode d'oscil�laci� (en Mil�lisegons!)
static char nIntensity = 0;     //Indicador d'intensitat dels LEDs
static char nTimer = -1;        //Handler del timer

/*
================================================================================
                             FUNCIONS I PROCEDIMENTS
================================================================================
*/

//Pre: Cap
//Post: Inicialitza el TAD HeartBeat amb les configuracions que permeten fer
//canviar els LEDS d'intensitat de manera creixent i decreixent amb per�ode per
//defecte de 120 ms
void T_HeartBeat_Init(void){

    nTimer = T_Timers_GetTimer();
    if(nTimer != -1){

        //Fixem m�quina d'estats al primer estat
        nEstat = 0;

        //Resetegem el Timer
        T_Timers_ResetTimer(nTimer);

    }

}

//Pre: Cap
//Post: Cap
void T_HeartBeat_StatusMachine(void){
    switch(nEstat){
        case 0:
            LEDS_SetIntensity(nIntensity);
            if(T_Timers_GetValue(nTimer) >= nPeriod / 20){
                nIntensity++;
                if(nIntensity > 10){
                    nEstat++;
                    nIntensity = 10;
                }
                T_Timers_ResetTimer(nTimer);
            }
            break;
        case 1:
            if(T_Timers_GetValue(nTimer) >= 800){
                nEstat++;
            }
            break;
        case 2:
            LEDS_SetIntensity(nIntensity);
            if(T_Timers_GetValue(nTimer) >= nPeriod / 20){
                nIntensity--;
                if(nIntensity < 0){
                    nEstat++;
                    nIntensity = 1;
                }
                T_Timers_ResetTimer(nTimer);
            }
            break;
         case 3:
            if(T_Timers_GetValue(nTimer) >= 800){
                nEstat = 0;
            }
            break;

    }
}


//Pre: nPeriod ha de valdre entre 0 i 32768
//Post: Fixa el per�ode de canvi d'intensitat al valor rebut per par�metre.
void T_HeartBeat_SetPeriod(unsigned int nValue){
    nPeriod = nValue;
}
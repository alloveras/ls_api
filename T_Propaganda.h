/*
================================================================================
 * Nom: T_PROPAGANDA
 * Fitxer: T_PROPAGANDA.h
 * Funcionalitat: TAD per mostrar la propaganda del display del robot.
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * �ltima Revisi�: 26/01/2015
================================================================================
*/

#ifndef _T_PROPAGANDA_H_
#define _T_PROPAGANDA_H_

//Includes de C
#include <stdio.h>

//Includes de l'API
#include "T_LCD.h"
#include "ACC.h"
#include "T_Timers.h"
#include "USB.h"

//Pre: Els TADs Timers, LCD i ACC han d'estar inicialitzats, aix� com els seus
//drivers associats.
//Post: Inicialitza el TAD propaganda
void T_PROPAGANDA_Init(void);

//Pre: Cap
//Post: Cap
void T_PROPAGANDA_StatusMachine(void);

#endif

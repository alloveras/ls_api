/*
================================================================================
 * Nom: UART
 * Fitxer: UART.c
 * Funcionalitat: Driver del canal s�rie del robot.
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * �ltima Revisi�: 28/01/2015
================================================================================
*/

//Include de les cap�aleres
#include "UART.h"

/*
================================================================================
                              CONSTANTS PRIVADES
================================================================================
*/

#define MAX_QUEUE 8


/*
================================================================================
                               TIPUS PRIVATS
================================================================================
*/
typedef struct{
    unsigned char nStart;
    unsigned char nEnd;
    unsigned char nSize;
    char aQueue[MAX_QUEUE];
}UART_Queue;


/*
================================================================================
                            VARIABLES PRIVADES
================================================================================
*/

static UART_Queue RXQueue;
static UART_Queue TXQueue;
static unsigned char nEstat = 0;

/*
================================================================================
                      PROCEDIMENTS I FUNCIONS PRIVADES
================================================================================
*/

static char Pop(UART_Queue *cua){
    char cReturn = ' ';
    if(cua->nSize > 0){
        cReturn = cua->aQueue[cua->nStart];
        cua->nStart++;
        if(cua->nStart == MAX_QUEUE) cua->nStart = 0;
        cua->nSize--;
    }
    return cReturn;
}


static void Push(UART_Queue *cua,char byte){
    if(cua->nSize < MAX_QUEUE){
        cua->aQueue[cua->nEnd] = byte;
        cua->nEnd++;
        if(cua->nEnd == MAX_QUEUE) cua->nEnd = 0;
        cua->nSize++;
    }else{
        Pop(cua);
        cua->aQueue[cua->nEnd] = byte;
        cua->nEnd++;
        if(cua->nEnd == MAX_QUEUE) cua->nEnd = 0;
        cua->nSize++;
    }
}

void __attribute__((interrupt, no_auto_psv)) _U2RXInterrupt(void) {
    char data;
    IFS1bits.U2RXIF = 0;
    if (U2STAbits.OERR == 1) U2STAbits.OERR = 0;
    data = U2RXREG;
    Push(&RXQueue,data);    //Encuem a la cua de recepci�
}

/*
================================================================================
                      PROCEDIMENTS I FUNCIONS P�BLICS
================================================================================
*/

//Pre: Cap
//Post: Inicialitza la UART amb un baudrate de 9600, transmissi� de 8 bits
//sense paritat i amb un stop-bit.
void UART_Init(void){

    TXQueue.nEnd = 0;
    TXQueue.nStart = 0;
    TXQueue.nSize = 0;

    RXQueue.nEnd = 0;
    RXQueue.nSize = 0;
    RXQueue.nStart = 0;

    U2MODE = 0x8000;
    U2STA = 0x2400;
    U2BRG = 103;
    IEC1bits.U2RXIE = 1;
    IPC7bits.U2RXIP = 6;

    nEstat = 0;
}

//Pre: Cap
//Post: Cap
void UART_StatusMachine(void){

    switch(nEstat){
        case 0:
            if(TXQueue.nSize > 0){
                U2TXREG = Pop(&TXQueue);
                nEstat++;
            }
            break;
        case 1:
            if(U2STAbits.TRMT == 1){
                nEstat = 0;
            }
            break;
    }


}

//Pre: S'ha d'haver comprovat pr�viament que la cua de sortida no estigui plena.
//Post: Encua el byte rebut per par�metre a la cua de sortida de la UART per
//tal d'enviar-lo quan sigui possible.
void UART_WriteByte(char byte){
    Push(&TXQueue,byte);
}

//Pre: S'ha d'haver comprovat que hi ha almenys un byte a la cua d'entrada de la
//UART.
//Post: Retorna el primer byte encuat a la cua d'entrda de la UART. S'ha de
//tenir en compte que la lectura �s DESTRUCTIVA.
char UART_ReadByte(void){
    char nReturn = ' ';
    if(RXQueue.nSize > 0){
        nReturn = Pop(&RXQueue);
    }
    return nReturn;
}

//Pre: Cap
//Post: Retorna 1 (CERT) si la cua de sortida de la UART no t� espai disponible
//per un nou byte a enviar. Altrament, aquesta funci� retornar� 0 (FALS).
unsigned char UART_OutputQueueFull(void){
    return TXQueue.nSize == MAX_QUEUE;
}

//Pre: Cap
//Post: Retorna 1 (CERT) si la cua d'entrada de la UART te al menys 1 byte
//pendent de ser llegit. En qualsevol altre cas, aquesta funci� retornar� 0
//(FALS).
unsigned char UART_InputQueueEmpty(void){
    return RXQueue.nSize == 0;
}

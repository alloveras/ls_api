/*
================================================================================
 * Nom: USB
 * Fitxer: USB.h
 * Funcionalitat: Driver per poder enviar i rebre dades a trav�s de bus USB que
 * incorpora el robot.
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * �ltima Revisi�: 27/01/2015
================================================================================
*/

#ifndef _USB_H_
#define _USB_H_

#include <xc.h>

//Pre: Cap
//Post: Inicialitza el bus USB amb un baudrate de 9600, transmissi� de 8 bits
//sense paritat i amb un stop-bit.
void USB_Init(void);

//Pre: Cap
//Post: Cap
void USB_StatusMachine(void);

//Pre: S'ha d'haver comprovat pr�viament que la cua de sortida no estigui plena.
//Post: Encua el byte rebut per par�metre a la cua de sortida del bus USB per
//tal d'enviar-lo quan sigui possible.
void USB_WriteByte(char byte);

//Pre: S'ha d'haver comprovat que hi ha almenys un byte a la cua d'entrada del
//bus USB.
//Post: Retorna el primer byte encuat a la cua d'entrda del bus USB. S'ha de
//tenir en compte que la lectura �s DESTRUCTIVA.
char USB_ReadByte(void);

//Pre: Cap
//Post: Retorna 1 (CERT) si la cua de sortida del bus USB no t� espai disponible
//per un nou byte a enviar. Altrament, aquesta funci� retornar� 0 (FALS).
unsigned char USB_OutputQueueFull(void);

//Pre: Cap
//Post: Retorna 1 (CERT) si la cua d'entrada del bus USB te al menys 1 byte
//pendent de ser llegit. En qualsevol altre cas, aquesta funci� retornar� 0
//(FALS).
unsigned char USB_InputQueueEmpty(void);


#endif
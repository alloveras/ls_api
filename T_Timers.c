/*
================================================================================
 * Nom: TAD TIMERS
 * Fitxer: T_TIMERS.c
 * Funcionalitat: Gestor de Timers de l'LS Maker
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * �ltima Revisi�: 26/01/2015
================================================================================
*/

//Includes de les cap�aleres
#include "T_Timers.h"

/*
================================================================================
                            CONSTANTS PRIVADES
================================================================================
*/

#define MAX_TIMERS 32      //Nombre m�xim de timers simultanis dincs del LSMaker
#define MAX_TIMER_VALUE 30000 //Nombre maxim dels tics universals (INT Overflow)
#define TRUE 1                  //Definici� de CERT
#define FALSE 0                 //Definici� de FALS

/*
================================================================================
                         DEFINICI� DE TIPUS PRIVATS
================================================================================
*/
struct{
    unsigned int nInitialValue;
    unsigned char bIsBusy;
}s_Timers[MAX_TIMERS];

/*
================================================================================
                             VARIABLES PRIVADES
================================================================================
*/

static unsigned int nGlobalTimer;       //Comptador de tics universals

/*
================================================================================
 RUTINA DE SERVEI D'INTERRUPCI� TIMER1 (MAXIMA PRIORITAT)
================================================================================
*/


//Pre : Cap
//Post: Cap
void __attribute__((__interrupt__, __auto_psv__)) _T1Interrupt(void){

    //Declarem variables
    unsigned char nCounter = 0;

    //Resetegem el flag de la interrupci�
    IFS0bits.T1IF = 0;
    
    //Resetegem el comptador del timer
    TMR1 = 0x00;

    //Incrementem Tics Universals
    nGlobalTimer++;

    //Comprovem si la variable nTicsUniversals esta aprop de l'overflow
    if (nGlobalTimer >= MAX_TIMER_VALUE) {

        //Si es dona el cas, abans de que succeeixi l'overflow, els tirem
        //tots avall
        for (nCounter = 0; nCounter < MAX_TIMERS ; nCounter++){
             if (s_Timers[nCounter].bIsBusy == TRUE){
                 s_Timers[nCounter].nInitialValue -= nGlobalTimer;
             }
        }
        nGlobalTimer = 0;
    }

}

//Pre: Cap
//Post: Inicialitza el TAD Timer perqu� proveeixi a l'usuari amb MAX_TIMERS i
//pre estableix que cada pas d'aquests timers ser� de 1ms. Paral�lelament,
//configura la interupci� d'aquest Timer com a interrupci� de m�xima prioritat,
//ja que NINGU pot retrassar el pas del temps.
void T_Timers_Init(void) {

    //Declarem variables
    unsigned char nCounter;

    //Alliberem tots els Timers
    for(nCounter = 0; nCounter < MAX_TIMERS ; nCounter++){
        s_Timers[nCounter].bIsBusy = FALSE;
    }

    //Resetegem el valor dels Tics Universals
    nGlobalTimer = 0;

    //Configurem el Timer1
    T1CONbits.TON = 1;
    T1CONbits.TSIDL = 0;
    T1CONbits.TGATE = 0;
    T1CONbits.TCKPS0 = 0;
    T1CONbits.TCKPS1 = 0;
    T1CONbits.T1SYNC = 0;
    T1CONbits.TCS = 0;

    //Carreguem el periode d'interrupcio al timer
    PR1 = 16000;   //--> A 32MHz per interrompr� cada 1ms
    TMR1 = 0x00;

    //Activem la interrupcio del Timer1
    IEC0bits.T1IE = 1;

    //Resetegem el flag d'interrupcio del Timer1
    IFS0bits.T1IF = 0;

    //Configurem la prioritat del Timer1 a nivell 7 (la mes alta)
    IPC0bits.T1IP0 = 1;
    IPC0bits.T1IP1 = 1;
    IPC0bits.T1IP2 = 1;

}

//Pre: Nom�s es pot cridar dins de rutines d'inicialitzaci�, sin� encallar� els
//motors coperatius.
//Post : Retorna un handler que identifica el Timer que se'ns ha associat (el
//valor retornat val entre 0 i 127. Si el valor retornat �s -1 llavors �s senyal
//de que no hi havia timers disponibles al sistema.
char T_Timers_GetTimer(void) {

    //Declaraci� de variables
    unsigned char nCounter = 0;

    //Recorrem l'array de Timers disponibles cercant-ne un de lliure
    for(nCounter = 0 ; nCounter < MAX_TIMERS ; nCounter++){
        //Si est� lliure el posem a ocupat i retornem el handler del Timer
        if(s_Timers[nCounter].bIsBusy == FALSE){
            s_Timers[nCounter].bIsBusy = TRUE;
            return nCounter;
        }
    }

    //Si no hem trobat cap Timer lliure el handler val -1
    return -1;
}


//Pre : nHandler ha de ser un valor de Timer v�lid (0 - 127)
//Post : Reseteja el timer identificat per nHandler.
void T_Timers_ResetTimer (char nHandler) {
    if(nHandler >= 0 && nHandler < MAX_TIMERS){
        s_Timers[(unsigned char)nHandler].nInitialValue = nGlobalTimer;
    }
}


//Pre : nHandler ha de ser un valor de Timer v�lid (0 - 127)
//Post: Retorna la quantitat de passos que ha fet el timer identificat per el
//nHandler des de l'�ltim reset del mateix.
unsigned int T_Timers_GetValue(char nHandler) {
    //Retornem el valor de la resta entre nTicsUniversals i els tics inicials
    //del timer en q�esti�
    if(nHandler >= 0 && nHandler < MAX_TIMERS){
        return (nGlobalTimer-(s_Timers[(unsigned char)nHandler].nInitialValue));
    }
    return 0;
}

//Pre: nHandler ha de tenir un valor entre 0 i 127.
//Post: Allibera el timer que s'identifica amb el valor nHandler.
void T_Timers_FreeTimer(char nHandler) {
    if(nHandler >= 0 && nHandler < MAX_TIMERS){
        s_Timers[(unsigned char)nHandler].bIsBusy = FALSE;
    }
}


/*
================================================================================
 * Nom: DRIVER LEDS
 * Fitxer: LEDS.h
 * Funcionalitat: Controla els HeartBeat LEDS.
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * �ltima Revisi�: 11/10/2014
================================================================================
*/
#ifndef _LEDS_H_
#define _LEDS_H_

//Includes del MCU
#include <xc.h>

//Include drivers necess�ris
#include "T_Timers.h"

/*
================================================================================
                             FUNCIONS I PROCEDIMENTS
================================================================================
*/

//Pre: Cap
//Post: Inicialitza el TAD LEDS posant el port HeartBeat de sortida i posant
//l'estat de la m�quina d'estats del motor al estat 0.
void LEDS_Init(void);

//Pre : Cap
//Post: Cap
void LEDS_StatusMachine(void);

//Pre: nIntensity ha de valdre entre 1 i 10
//Post: Fixa la lluminositat dels LEDS de HeartBeat
void LEDS_SetIntensity(unsigned char nIntensity);

#endif



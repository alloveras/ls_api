/*
================================================================================
 * Nom: TAD TIMER
 * Fitxer: T_Timer.h
 * Funcionalitat: Gestor de Timers de l'LS Maker
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * �ltima Revisi�: 26/01/2015
================================================================================
*/

#ifndef _T_TIMERS_H_
#define _T_TIMERS_H_

//Include del MCU
#include <xc.h>

/*
================================================================================
                             FUNCIONS I PROCEDIMENTS
================================================================================
*/

//Pre: Cap
//Post: Inicialitza el TAD Timer perqu� proveeixi a l'usuari amb MAX_TIMERS i
//pre estableix que cada pas d'aquests timers ser� de 1ms. Paral�lelament,
//configura la interupci� d'aquest Timer com a interrupci� de m�xima prioritat,
//ja que NINGU pot retrassar el pas del temps.
void T_Timers_Init(void);

//Pre: Nom�s es pot cridar dins de rutines d'inicialitzaci�, sin� encallar� els
//motors coperatius.
//Post : Retorna un handler que identifica el Timer que se'ns ha associat (el
//valor retornat val entre 0 i 127. Si el valor retornat �s -1 llavors �s senyal
//de que no hi havia timers disponibles al sistema.
char T_Timers_GetTimer(void);

//Pre : nHandler ha de ser un valor de Timer v�lid (0 - 127)
//Post : Reseteja el timer identificat per nHandler.
void T_Timers_ResetTimer (char nHandler);

//Pre : nHandler ha de ser un valor de Timer v�lid (0 - 127)
//Post: Retorna la quantitat de passos que ha fet el timer identificat per el
//nHandler des de l'�ltim reset del mateix.
unsigned int T_Timers_GetValue(char nHandler);

//Pre: nHandler ha de tenir un valor entre 0 i 127.
//Post: Allibera el timer que s'identifica amb el valor nHandler.
void T_Timers_FreeTimer(char nHandler);


#endif

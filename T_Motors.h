/*
================================================================================
 * Nom: TAD MOTORS
 * Fitxer: T_Motors.h
 * Funcionalitat: Permet la variaci� de la velocitat dels motors del tricopter
 * mitjan�ant la comunicaci� amb el MCU dedicat als PWM a trav�s del protocol
 * propi de LS_COPTER.
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * �ltima Revisi�: 29/01/2015
================================================================================
*/
/*
================================================================================
 * Nom: TAD MOTORS
 * Fitxer: T_Motors.c
 * Funcionalitat: Permet la variaci� de la velocitat dels motors del tricopter
 * mitjan�ant la comunicaci� amb el MCU dedicat als PWM a trav�s del protocol
 * propi de LS_COPTER.
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * �ltima Revisi�: 29/01/2015
================================================================================
*/

#ifndef _T_MOTORS_H_
#define _T_MOTORS_H_

#include <xc.h>
#include "USB.h"
#include "UART.h"
#include "T_Timers.h"
#include "LEDS.h"

//Pre: El TAD Timers ha d'estar inicialitzat
//Post: Demana un timer i inicialitza les cues del TAD Motors.
void T_MOTORS_Init(void);

//Pre: Cap
//Post: Cap
void T_MOTORS_StatusMachine(void);

#endif


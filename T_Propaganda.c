/*
================================================================================
 * Nom: T_PROPAGANDA
 * Fitxer: T_PROPAGANDA.h
 * Funcionalitat: TAD per mostrar la propaganda del display del robot.
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * �ltima Revisi�: 26/01/2015
================================================================================
*/

//Includes de les cap�aleres
#include "T_Propaganda.h"

/*
================================================================================
                             CONSTANTS PRIVADES
================================================================================
*/

#define NOM_ROBOT " LS_COPTER v1.0 "
#define NOM_UNIVERSITAT "    LA SALLE    "
#define NOM_PROPIETARI "   A.LLOVERAS   "

/*
================================================================================
                              VARIABLES PRIVADES
================================================================================
*/
unsigned char nEstat = 0;       //Indicador d'estat de la m�quina d'estats
unsigned char nTimer = -1;      //Handler del timer
unsigned char nCounter = 0;     //�ndex de prop�sit general
char nPosX = 0;                 //Orientaci� Vertical (Eix X)
char nPosY = 0;                 //Orientaci� Horitzontal (Eix Y)
char nPosZ = 0;                 //Orientaci� Respecte l'Eix Z
char aDadesACC[16];             //Cadena de dades de l'Acceler�metre

/*
================================================================================
                    PROCEDIMENTS I FUNCIONS TAD PROPAGANDA
================================================================================
*/

//Pre: Els TADs Timers, LCD i ACC han d'estar inicialitzats, aix� com els seus
//drivers associats.
//Post: Inicialitza el TAD propaganda
void T_PROPAGANDA_Init(void){

    nTimer = T_Timers_GetTimer();

    if(nTimer != -1){
        //Copiem les dades a la cadena a printar per pantalla
        sprintf(aDadesACC,"%s","  X:     Y:     ");

        //Inicialitzem variables
        nEstat = 0;
        nCounter = 0;
        
        //Reiniciem el timer
        T_Timers_ResetTimer(nTimer);

    }

}

//Pre: Cap
//Post: Cap
void T_PROPAGANDA_StatusMachine(void){

    switch(nEstat){
        case 0:
            if(T_Timers_GetValue(nTimer) >= 400){
                if(T_LCD_FirstPosition() == 1){
                    ACC_GetXYZ(&nPosX,&nPosY,&nPosZ);
                    nCounter = 0;
                    nEstat++;
                }
            }
            break;
        case 1:
            if(nCounter < 16){
                if(T_LCD_FullQueue() == 0){
                   T_LCD_PutChar(NOM_ROBOT[nCounter]);
                    nCounter++;
                }
                nEstat = 1;
            }else{
                nCounter = 0;
                sprintf(aDadesACC+4,"%c%c%c",' ',' ',' ');
                sprintf(aDadesACC+11,"%c%c%c",' ',' ',' ');
                sprintf(aDadesACC+4,"%d",nPosX);
                sprintf(aDadesACC+11,"%d",nPosY);
                nEstat++;
            }
            break;
        case 2:
            if(nCounter < 16){
                if(aDadesACC[nCounter] == '\0') aDadesACC[nCounter] = ' ';
                nCounter++;
                nEstat = 2;
            }else{
                nCounter = 0;
                nEstat++;
            }
            break;
        case 3:
            if(nCounter < 16){
                if(T_LCD_FullQueue() == 0){
                    T_LCD_PutChar(aDadesACC[nCounter]);
                    nCounter++;
                }
                nEstat = 3;
            }else{
                nEstat++;
                nCounter = 0;
            }
            break;
        case 4:
            if(nCounter < 16){
                if(T_LCD_FullQueue() == 0){
                    T_LCD_PutChar(NOM_UNIVERSITAT[nCounter]);
                    nCounter++;
                }
                nEstat = 4;
            }else{
                nEstat++;
                nCounter = 0;
            }
            break;
        case 5:
            if(nCounter < 16){
                if(T_LCD_FullQueue() == 0){
                    T_LCD_PutChar(NOM_PROPIETARI[nCounter]);
                    nCounter++;
                }
                nEstat = 5;
            }else{
                nCounter = 0;
                nEstat = 0;
                T_Timers_ResetTimer(nTimer);
            }
            break;

    }

}

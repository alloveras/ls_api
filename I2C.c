/*
================================================================================
 * Nom: DRIVER I2C
 * Fitxer: I2C.c
 * Funcionalitat: Controla l'acc�s als busos I2C del robot
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * �ltima Revisi�: 26/05/2015
================================================================================
*/

//Include del fitxer de cap�aleres
#include "I2C.h"

/*
================================================================================
                            CONSTANTS PRIVADES
================================================================================
*/

#define MAX_QUEUE 16        //Mida de la cua de sortida del bus I2C

/*
================================================================================
                              TIPUS PRIVATS
================================================================================
*/

typedef struct{
    char cDeviceAddr;
    char cRegisterAddr;
    char cData;
}I2C_Packet_Write;

typedef struct{
    char cDeviceAddr;
    char cRegisterAddr;
    char *cData;
}I2C_Packet_Read;

typedef struct{
    I2C_Packet_Write aQueue[MAX_QUEUE];
    unsigned char nStart;
    unsigned char nEnd;
    unsigned char nSize;
}I2C_Queue;

static unsigned char nTimer1 = -1;  //Handler del timer
static unsigned char nEstat1 = 0;   //Estat de la m�quina d'estats del driver
static unsigned char isReceiving = 0;   //Boole� pel control de recepci�
static I2C_Queue qOutput1;      //Cua de sortida de paquets I2C
static I2C_Packet_Read pRead;   //Auxiliar que aguanta les dades rebudes per I2C

/*
================================================================================
                       FUNCIONS I PROCEDIMENTS P�BLICS
================================================================================
*/

//Pre: El TAD Timers ha d'estar inicialitzat
//Post: Configura els registres de l'MCU per activar el bus I2C1 funcionant en
//mode m�ster i amb una velocitat de 400Kbps
void I2C1_Init(void){

    char temp;
    nTimer1 = T_Timers_GetTimer();

    if(nTimer1 != -1){

       I2C1BRG = 0x25;
       I2C1CONbits.I2CEN = 0;	//Desactivem I2C
       I2C1CONbits.DISSLW = 1;	//Desactivem SlewRate
       IFS1bits.MI2C1IF = 0;	//Tanquem Interrupci�
       I2C1CONbits.I2CEN = 1;	//Activem I2C


       temp = I2C1RCV;          //Buidem Buffer per si estava pl�

       I2C1CONbits.PEN = 1;
       while(I2C1CONbits.PEN);  //Esperem a que s'envi�n StopBits

       I2C1CONbits.RCEN = 0;    //Activem recepci�
       IFS1bits.MI2C1IF = 0;    //Netegem flag d'interrupci�
       I2C1STATbits.IWCOL = 0;
       I2C1STATbits.BCL = 0;

       //Esperem 1 ms de calma
       T_Timers_ResetTimer(nTimer1);
       while(T_Timers_GetValue(nTimer1) < 1);

    }
}

//Pre: S'ha d'haver comprovat que la cua de sortida del bus I2C t� espai
//t� espai suficient per albergar un paquet I2C m�s.
//Post: Encua el paquet I2C a la cua de sortida per ser enviar el m�s aviat
//possible.
void I2C1_WriteByte(char cDeviceAddr,char cRegisterAddr,char byte){
    if(qOutput1.nSize < MAX_QUEUE){
        qOutput1.aQueue[qOutput1.nEnd].cData = byte;
        qOutput1.aQueue[qOutput1.nEnd].cDeviceAddr = cDeviceAddr;
        qOutput1.aQueue[qOutput1.nEnd].cRegisterAddr = cRegisterAddr;
        qOutput1.nEnd++;
        if(qOutput1.nEnd == MAX_QUEUE) qOutput1.nEnd = 0;
        qOutput1.nSize++;
    }
}

//Pre: S'ha d'haver comprovat pr�viament que no s'est� rebent cap altre dada a
//trav�s del bus I2C.
//Post: Encua la petici� de lectura d'un registre d'un dispositiu remot a la
//llista de tasques del driver. Mentre aquesta no es realitzi, es descartar�
//qualsevol altre petici� de lectura. D'aquesta manera, quan la lectura acabi,
//ja sigui satisfact�riament o no, el bus I2C tornar� a estar llest per rebre
//una petici� de lectura.
void I2C1_ReadByte(char cDeviceAddr, char cRegisterAddr, char *byte){
    if(isReceiving == 0){
        isReceiving = 1;
        pRead.cData = byte;
        pRead.cDeviceAddr = cDeviceAddr;
        pRead.cRegisterAddr = cRegisterAddr;
    }
}

//Pre : Cap
//Post: Retornar� 1 (CERT) si hi ha alguna petici� de lectura pendent per
//processar-se. En qualsevol altre cas, retornar� 0 (FALS).
unsigned char I2C1_isReceiving(void){
    return isReceiving;
}

//Pre: Cap
//Post: Retornar� 1 (CERT) si la cua de sortida del bus I2C cont� espai per
//tants paquets I2C com nItems s'hagin especificat. Altrament, retornar� 0 que
//equival a (FALS).
char I2C1_OutputQueueHasSpace(unsigned char nItems){
    return qOutput1.nSize + nItems <= MAX_QUEUE;
}

//Pre: Cap
//Post: Cap
void I2C1_StatusMachine(void){

    switch(nEstat1){
        case 0:
            if(qOutput1.nSize > 0){
                nEstat1 = 2;
            }else{
                nEstat1++;
            }
            break;
        case 1:
            if(isReceiving == 1){
                nEstat1 = 11;
            }else{
                nEstat1 = 0;
            }
            break;

       //ENVIAMENT DE DADES
        case 2:
            //Esperem que el bus estigui lliure
            if(I2C1STATbits.TRSTAT == 0){
                //Activem seq��ncia d'start bits
                I2C1CONbits.SEN = 1;
                nEstat1++;
            }
            break;
        case 3:
            //Esperem a que es completi la seq��ncia
            if(I2C1CONbits.SEN == 0){
                //Enviem l'adre�a del dispositiu I2C
                I2C1TRN = (qOutput1.aQueue[qOutput1.nStart].cDeviceAddr << 1) & 0xFE;
                nEstat1++;
            }
            break;
        case 4:
            //Esperem a que es completi la transmissi�
            if(I2C1STATbits.TBF == 0){
                nEstat1++;
            }
            break;
        case 5:
            //Esperem que el bus quedi lliure
            if(I2C1STATbits.TRSTAT == 0){
                //Comprovem l'ACK
                if(I2C1STATbits.ACKSTAT == 1){
                    //No tenim ACK
                    nEstat1 = 9;
                }else{
                    //Tenim ACK
                    I2C1TRN = qOutput1.aQueue[qOutput1.nStart].cRegisterAddr;
                    nEstat1++;
                }
            }
            break;
        case 6:
            //Esperem a que s'envi�n les dades
            if(I2C1STATbits.TBF == 0){
                nEstat1++;
            }
            break;
        case 7:
            //Esperem a que el bus quedi lliure
            if(I2C1STATbits.TRSTAT == 0){
                //Comprovem l'ACK
                if(I2C1STATbits.ACKSTAT == 1){
                    //No tenim ACK
                    nEstat1 = 9;
                }else{
                    //Tenim ACK
                    I2C1TRN = qOutput1.aQueue[qOutput1.nStart].cData;
                    nEstat1++;
                }
            }
            break;

        case 8:
            //Esperem que acabi la transmissi�
            if(I2C1STATbits.TBF == 0){
                nEstat1++;
            }
            break;
        case 9:
            //Espero que el bus quedi lliure
            if(I2C1STATbits.TRSTAT == 0){
                //No cal comprovar l'ACK perqu� tant si arriba com si no haurem
                //d'enviar la seq��ncia d'stop bits
                I2C1CONbits.PEN = 1;
                nEstat1++;
            }
            break;
        case 10:
            //Esperem a que s'envi� la seq��ncia d'stopbits
            if(I2C1CONbits.PEN == 0){
                qOutput1.nStart++;
                if(qOutput1.nStart == MAX_QUEUE) qOutput1.nStart = 0;
                qOutput1.nSize--;
                nEstat1 = 1;
            }
            break;

       //RECEPCI� DE DADES
        case 11:
            //Esperem que el bus quedi lliure
            if(I2C1STATbits.TRSTAT == 0){
                //Enviem seq��ncia d'start bits
                I2C1CONbits.SEN = 1;
                nEstat1++;
            }
            break;
        case 12:
            //Esperem a que s'envi�n els start bits
            if(I2C1CONbits.SEN == 0){
                I2C1TRN = (pRead.cDeviceAddr << 1) & 0xFE;
                nEstat1++;
            }
            break;
        case 13:
            //Esperem que s'envi�n les dades
            if(I2C1STATbits.TBF == 0){
                nEstat1++;
            }
            break;
        case 14:
            //Esperem que el bus quedi lliure
            if(I2C1STATbits.TRSTAT == 0){
                //Comprovem l'ACK
                if(I2C1STATbits.ACKSTAT == 1){
                    //No tenim ACK
                    nEstat1 = 22;
                }else{
                    //Tenim ACK
                    I2C1TRN = pRead.cRegisterAddr;
                    nEstat1++;
                }
            }
            break;
        case 15:
            //Esperem que s'envi�n les dades
            if(I2C1STATbits.TBF == 0){
                nEstat1++;
            }
            break;

        case 16:
            //Esperem que el bus quedi lliure
            if(I2C1STATbits.TRSTAT == 0){
                //Comprovem l'ACK
                if(I2C1STATbits.ACKSTAT == 1){
                    //No tenim ACK
                    nEstat1 = 22;
                }else{
                    //Tenim ACK
                    //Comencem seq��ncia de Restart
                    I2C1CONbits.RSEN = 1;	
                    nEstat1++;
                }
            }
            break;
        case 17:
            //Esperem a que acabi l'enviament de la seq��ncia de restart
            if(I2C1CONbits.RSEN == 0){
                //Enviem l'adre�a del dispositiu I2C
                I2C1TRN = (pRead.cDeviceAddr << 1) | 0x01;
                nEstat1++;
            }
            break;
        case 18:
            //Esperem a que s'envi�n les dades
            if(I2C1STATbits.TBF == 0){
                nEstat1++;
            }
            break;
        case 19:
            //Esperem que el bus quedi lliure
            if(I2C1STATbits.TRSTAT == 0){
                //Comprovem l'ACK
                if(I2C1STATbits.ACKSTAT == 1){
                    //No tenim ACK
                    nEstat1 = 22;
                }else{
                    //Tenim ACK
                    //Activem recepci� de dades
                    I2C1CONbits.RCEN = 1;
                    nEstat1++;
                }
            }
            break;
        case 20:
            //Esperem a que s'ompli el buffer de recepci�
            if(I2C1STATbits.RBF == 1){
                //Copiem dades i ens preparem per enviar NotACK
                (*pRead.cData) = I2C1RCV;
                I2C1CONbits.ACKDT = 1;
                I2C1CONbits.ACKEN = 1;
                nEstat1++;
            }
            break;
        case 21:
            //Esperem que es completi el NotACK
            if(I2C1CONbits.ACKEN == 0){
                I2C1CONbits.ACKDT = 0;
                nEstat1++;
            }
            break;
        case 22:
            //Enviem stop bits
            I2C1CONbits.PEN = 1;
            nEstat1++;
            break;
        case 23:
            //Esperem a que es completi l'enviament
            if(I2C1CONbits.PEN == 0){
                isReceiving = 0;
                nEstat1 = 0;
            }
            break;
    }


       
}

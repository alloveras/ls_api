/*
================================================================================
 * Nom: TAD LCD
 * Fitxer: T_LCD.c
 * Funcionalitat: Interf�cie de control de l'LCD del robot.
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * �ltima Revisi�: 26/01/2015
================================================================================
*/

//Includes de les cap�aleres
#include "T_LCD.h"

/*
================================================================================
                              CONSTANTS PRIVADES
================================================================================
*/

#define LCD_ADDR 0x40 >> 1      //Adre�a I2C de l'LCD
#define LCD_IODIRA 0x00         //Registre port de dades D[0..7]
#define LCD_IODIRB 0x01         //Registre port de control RS,R/!W i Enable
#define LCD_IPOLA 0x02          //Registre de control de polaritat PORTA
#define LCD_IPOLB 0x03          //Registre de control de polaritat PORTB
#define LCD_GPINTENA 0x04       //Registre de control d'interrupcions PORTA
#define LCD_GPINTENB 0x05       //Registre de control d'interrupcions PORTB
#define LCD_DEFVALA 0x06        //Registre de configuraci� valor inicial PORTA
#define LCD_DEFVALB 0x07        //Registre de configuraci� valor inicial PORTB
#define LCD_INTCONA 0x08        //Registre de configuraci� d'interrupcions PORTA
#define LCD_INTCONB 0x09        //Registre de configuraci� d'interrupcions PORTB
#define LCD_IOCON 0x0A          //Registre de configuraci� de l'expansor de pins
#define LCD_GPPUA 0x0C          //Registre de configuraci� de PULL-UPs PORTA
#define LCD_GPPUB 0x0D          //Registre de configuraci� de PULL-UPs PORTB
#define LCD_INTFA 0x0E          //Registre de configuraci� d'interrupcions PORTA
#define LCD_INTFB 0x0F          //Registre de configuraci� d'interrupcions PORTB
#define LCD_INTCAPA 0x10        //Registre de configuraci� d'interrupcions PORTA
#define LCD_INTCAPB 0x11        //Registre de configuraci� d'interrupcions PORTB
#define LCD_GPIOA 0x12          //Registre de lectura dels valors del PORTA
#define LCD_GPIOB 0x13          //Registre de lectura dels valors del portB
#define LCD_OLATA 0x14          //Registre d'escriptura dels valors del PORTA
#define LCD_OLATB 0x15          //Registre d'escriptura dels valors del PORTB

#define MAX_QUEUE 64            //Mida cua de sortida de l'LCD
#define MAX_COLUMNES 16         //Mida m�xima en columnes del display
#define TRUE 1                  //Definici� de CERT
#define FALSE 0                 //Definici� de FALS

/*
================================================================================
                                TIPUS PRIVATS
================================================================================
*/

typedef struct{
    char byte;
    unsigned char isInstruction;
    unsigned char nDelay;
}LCD_Instruction;

typedef struct{
    unsigned char nStart;
    unsigned char nEnd;
    unsigned char nSize;
    LCD_Instruction aQueue[MAX_QUEUE];
}LCD_Queue;

/*
================================================================================
                               VARIABLES PRIVADES
================================================================================
*/

static unsigned char nTimer = -1;    //Handler del timer
static unsigned char nFila = 0;      //Indicador de fila de l'LCD
static unsigned char nColumna = 0;   //Indicador de columna de l'LCD
static unsigned char nEstat = 0;     //Indicador d'estat de la m�quina d'estats
static LCD_Queue qCharacters;        //Cua de sortida de l'LCD

/*
================================================================================
                          FUNCIONS I PROCEDIMENTS PRIVATS
================================================================================
*/

//Pre: S'ha d'haver comprovat que hi hagi espai a la cua de sortida de l'LCD.
//Post: Encua una instrucci�/car�cter a la cua de sortida de l'LCD. En funci�
//del tipus de paquet encuat, l'actuaci� del display ser� d'interpretar la
//comanda o de mostrar el car�cter a la pantalla.
static void T_LCD_Push(unsigned char isInstruction,char byte,unsigned char nDelay){
    if(qCharacters.nSize < MAX_QUEUE){
        qCharacters.aQueue[qCharacters.nEnd].byte = byte;
        qCharacters.aQueue[qCharacters.nEnd].isInstruction = isInstruction;
        qCharacters.aQueue[qCharacters.nEnd].nDelay = nDelay;
        qCharacters.nEnd++;
        if(qCharacters.nEnd == MAX_QUEUE) qCharacters.nEnd = 0;
        qCharacters.nSize++;
    }
}

//Pre: S'ha d'haver comprovat pr�viament que a la cua hi havia informaci� per
//consultar.
//Post: Retorna el primer paquet de la cua de sortida de l'LCD.
static LCD_Instruction T_LCD_Top(){
    LCD_Instruction iReturn;
    if(qCharacters.nSize > 0){
        iReturn = qCharacters.aQueue[qCharacters.nStart];
    }
    return iReturn;
}

//Pre: S'ha d'haver comprovat pr�viament que a la cua hi havia informaci� per
//desencuar.
//Post: Desencua el primer paquet de la cua de sortida de l'LCD.
static void T_LCD_Pop(){
    if(qCharacters.nSize > 0){
        qCharacters.nStart++;
        if(qCharacters.nStart == MAX_QUEUE) qCharacters.nStart = 0;
        qCharacters.nSize--;
    }
}

//Pre: Cap
//Post: Retorna un byte que cont� els bits del byte rebut per par�metre per�
//en ordre invers. �s a dir, realitza un canvi de MSB a LSB.
static char T_LCD_GiraByte(char byte){
   byte = (byte & 0xF0) >> 4 | (byte & 0x0F) << 4;
   byte = (byte & 0xCC) >> 2 | (byte & 0x33) << 2;
   byte = (byte & 0xAA) >> 1 | (byte & 0x55) << 1;
   return byte;
}

/*
================================================================================
                          FUNCIONS I PROCEDIMENTS P�BLICS
================================================================================
*/

//Pre: Cap
//Post: Retorna 1 (CERT) si la cua de sortida de l'LCD est� plena. En qualsevol
//altre cas, retorna 0 (FALS).
unsigned char T_LCD_FullQueue(){
    return qCharacters.nSize == MAX_QUEUE;
}

//Pre: Cap
//Post: Retorna 1 (CERT) si s'ha pogut posar el cursor a la primera posici� de
//la pantalla LCD. En qualsevol altre cas, el valor de retorn ser� 0 (FALS).
unsigned char T_LCD_FirstPosition(){
    if(qCharacters.nSize + 1 <= MAX_QUEUE){
        nFila = 0;
        nColumna = 0;
        T_LCD_Push(TRUE,T_LCD_GiraByte(0x80),4);
        return 1;
    }else{
        return 0;
    }
}

//Pre: Cap
//Post: Retorna 1 (CERT) si s'ha pogut netejar la pantalla LCD i 0 (FALS) en
//qualsevol altre cas.
unsigned char T_LCD_ClearScreen(){
    if(qCharacters.nSize + 2 <= MAX_QUEUE){
        nFila = 0;
        nColumna = 0;
        T_LCD_Push(TRUE,T_LCD_GiraByte(0x01),4);
        T_LCD_Push(TRUE,T_LCD_GiraByte(0x80),4);
        return 1;
    }else{
        return 0;
    }
}

//Pre: S'ha d'haver comprovat que hi ha espai disponible a la cua de sortida de
//l'LCD.
//Post: Encua el car�cter a la cua de sortida de l'LCD per ser printat pel
//display quan sigui possible.
void T_LCD_PutChar(char byte){
    if(qCharacters.nSize < MAX_QUEUE){
        T_LCD_Push(FALSE,T_LCD_GiraByte(byte),1);
    }
}

//Pre: El TAD Timers ha d'estar inicialitzat.
//Post: Inicialitza el display del robot fent la seq��ncia d'inicialitzaci�
//corresponent i fixant les configuracions de cursor amagat, autoshiftat a
//l'esquerra i LCD activat per escriptura.
void T_LCD_Init(void){

    //Declarem variables

    nTimer = T_Timers_GetTimer();
    
    if(nTimer != -1){

        //Resetegem les files i les columnes
        nFila = 0;
        nColumna = 0;

        //Inicialitzem el buffer de sortida
        qCharacters.nSize = 0;
        qCharacters.nStart = 0;
        qCharacters.nEnd = 0;

        //Configurem el MCP23017
        I2C1_WriteByte(LCD_ADDR,LCD_IOCON,0b00111010);

        //Posem tots els ports de sortida
        I2C1_WriteByte(LCD_ADDR,LCD_IODIRA,0x00);
        I2C1_WriteByte(LCD_ADDR,LCD_IODIRB,0x00);
        I2C1_WriteByte(LCD_ADDR,LCD_IPOLA,0x00);
        I2C1_WriteByte(LCD_ADDR,LCD_IPOLB,0x00);
        I2C1_WriteByte(LCD_ADDR,LCD_GPINTENA,0x00);
        I2C1_WriteByte(LCD_ADDR,LCD_GPINTENB,0x00);

        I2C1_WriteByte(LCD_ADDR,LCD_OLATA,0x00);
        I2C1_WriteByte(LCD_ADDR,LCD_OLATB,0x00);

        //Tai i com va dir en Sisco el seu dia... M�s val repetir  la
        //seq��ncia d'inicialitzaci� dues vegades, ja que sin�, a vegades,
        //despr�s d'una arrancada freda, el LCD es queda boig...
        for(nFila = 0 ; nFila < 2 ; nFila ++){
            //Encuem la seq��ncia d'inicialitzaci� del display
            T_LCD_Push(TRUE,0x0C,5);
            T_LCD_Push(TRUE,0x0C,1);
            T_LCD_Push(TRUE,0x0C,1);
            T_LCD_Push(TRUE,0x0C,1);
            T_LCD_Push(TRUE,0x1C,1);
            T_LCD_Push(TRUE,0x10,1);
            T_LCD_Push(TRUE,0x80,1);
            T_LCD_Push(TRUE,0x60,1);
            T_LCD_Push(TRUE,0x30,1);
        }

        nFila = 0;

        T_Timers_ResetTimer(nTimer);

    }
}

//Pre: Cap
//Post: Cap
void T_LCD_StatusMachine(void){
    switch(nEstat){

        case 0:
            if(T_Timers_GetValue(nTimer) >= 100){
                nEstat++;
            }
            break;
        case 1:
            //Mirem si hi ha feina per fer
            if(qCharacters.nSize > 0){
                if(I2C1_OutputQueueHasSpace(3)){
                    if(T_LCD_Top().isInstruction == 1){
                        I2C1_WriteByte(LCD_ADDR,LCD_OLATB,0x04);
                        I2C1_WriteByte(LCD_ADDR,LCD_OLATA,T_LCD_Top().byte);
                        I2C1_WriteByte(LCD_ADDR,LCD_OLATB,0x00);
                    }else{
                        I2C1_WriteByte(LCD_ADDR,LCD_OLATB,0x05);
                        I2C1_WriteByte(LCD_ADDR,LCD_OLATA,T_LCD_Top().byte);
                        I2C1_WriteByte(LCD_ADDR,LCD_OLATB,0x01);
                        nColumna++;
                    }
                    nEstat++;
                    T_Timers_ResetTimer(nTimer);
                }
            }
            break;
        case 2:
            if(T_Timers_GetValue(nTimer) >= T_LCD_Top().nDelay){
                T_LCD_Pop();
                if(nColumna == MAX_COLUMNES){
                    nColumna = 0;
                    nEstat++;
                }else{
                    nEstat = 1;
                }
            }
            break;
        case 3:
            if(I2C1_OutputQueueHasSpace(3)){
                 I2C1_WriteByte(LCD_ADDR,LCD_OLATB,0x04);
                 if(nFila == 0){
                    I2C1_WriteByte(LCD_ADDR,LCD_OLATA,T_LCD_GiraByte(0x40 | 0x80));
                    nFila++;
                 }else if(nFila == 1){
                    I2C1_WriteByte(LCD_ADDR,LCD_OLATA,T_LCD_GiraByte(0x10 | 0x80));
                    nFila++;
                 }else if(nFila == 2){
                     I2C1_WriteByte(LCD_ADDR,LCD_OLATA,T_LCD_GiraByte(0x50 | 0x80));
                     nFila++;
                 }else if(nFila == 3){
                     I2C1_WriteByte(LCD_ADDR,LCD_OLATA,T_LCD_GiraByte(0x00 | 0x80));
                     nFila = 0;
                 }
                 I2C1_WriteByte(LCD_ADDR,LCD_OLATB,0x00);
                 
                nEstat++;
                T_Timers_ResetTimer(nTimer);
            }
            break;
        case 4:
            if(T_Timers_GetValue(nTimer) >= 4){
                nEstat = 1;
            }
            break;
    }
}
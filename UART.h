/*
================================================================================
 * Nom: UART
 * Fitxer: UART.h
 * Funcionalitat: Driver del canal s�rie del robot.
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * �ltima Revisi�: 28/01/2015
================================================================================
*/

#ifndef _UART_H_
#define _UART_H_

#include <xc.h>

//Pre: Cap
//Post: Inicialitza la UART amb un baudrate de 9600, transmissi� de 8 bits
//sense paritat i amb un stop-bit.
void UART_Init(void);

//Pre: Cap
//Post: Cap
void UART_StatusMachine(void);

//Pre: S'ha d'haver comprovat pr�viament que la cua de sortida no estigui plena.
//Post: Encua el byte rebut per par�metre a la cua de sortida de la UART per
//tal d'enviar-lo quan sigui possible.
void UART_WriteByte(char byte);

//Pre: S'ha d'haver comprovat que hi ha almenys un byte a la cua d'entrada de la
//UART.
//Post: Retorna el primer byte encuat a la cua d'entrda de la UART. S'ha de
//tenir en compte que la lectura �s DESTRUCTIVA.
char UART_ReadByte(void);

//Pre: Cap
//Post: Retorna 1 (CERT) si la cua de sortida de la UART no t� espai disponible
//per un nou byte a enviar. Altrament, aquesta funci� retornar� 0 (FALS).
unsigned char UART_OutputQueueFull(void);

//Pre: Cap
//Post: Retorna 1 (CERT) si la cua d'entrada de la UART te al menys 1 byte
//pendent de ser llegit. En qualsevol altre cas, aquesta funci� retornar� 0
//(FALS).
unsigned char UART_InputQueueEmpty(void);


#endif
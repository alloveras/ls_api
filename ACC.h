/*
================================================================================
 * Nom: DRIVER ACCELER�METRE
 * Fitxer: UART.h
 * Funcionalitat: Permet llegir les dades de l'acceler�metre del robot a trav�s
 * del bus I2C.
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * �ltima Revisi�: 26/01/2015
================================================================================
*/

#ifndef _ACC_H_
#define _ACC_H_

//Includes del MCU
#include <xc.h>

//Includes de l'API
#include "I2C.h"

//Pre: El TAD Timers ha d'estar inicialitzat
//Post: Activa l'alimentaci� de l'acceler�metre a trav�s del transistor de
//control i fixa les configuracions necess�ries per obtenir orientaci� en els
//eixos X, Y i Z.
void ACC_Init(void);

//Pre: Els par�metres X, Y i Z han de ser punters a variables del tipus CHAR i
//MAI podr�n valdre NULL.
//Post: Assigna la quatitat de graus d'inclinaci� entre la placa del robot i
//els eixos de coordenades.
void ACC_GetXYZ(char *X, char *Y, char *Z);

//Pre: Cap
//Post: Cap
void ACC_StatusMachine(void);

#endif

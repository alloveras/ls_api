/*
================================================================================
 * Nom: TAD LCD
 * Fitxer: T_LCD.h
 * Funcionalitat: Interf�cie de control de l'LCD del robot.
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * �ltima Revisi�: 26/01/2015
================================================================================
*/

#ifndef T_DISPLAY_H
#define	T_DISPLAY_H

//Includes de l'API
#include "T_Timers.h"
#include "I2C.h"

//Pre: El TAD Timers ha d'estar inicialitzat.
//Post: Inicialitza el display del robot fent la seq��ncia d'inicialitzaci�
//corresponent i fixant les configuracions de cursor amagat, autoshiftat a
//l'esquerra i LCD activat per escriptura.
void T_LCD_Init(void);

//Pre: S'ha d'haver comprovat que hi ha espai disponible a la cua de sortida de
//l'LCD.
//Post: Encua el car�cter a la cua de sortida de l'LCD per ser printat pel
//display quan sigui possible.
void T_LCD_PutChar(char byte);

//Pre: Cap
//Post: Retorna 1 (CERT) si la cua de sortida de l'LCD est� plena. En qualsevol
//altre cas, retorna 0 (FALS).
unsigned char T_LCD_FullQueue();

//Pre: Cap
//Post: Retorna 1 (CERT) si s'ha pogut netejar la pantalla LCD i 0 (FALS) en
//qualsevol altre cas.
unsigned char T_LCD_ClearScreen();

//Pre: Cap
//Post: Retorna 1 (CERT) si s'ha pogut posar el cursor a la primera posici� de
//la pantalla LCD. En qualsevol altre cas, el valor de retorn ser� 0 (FALS).
unsigned char T_LCD_FirstPosition();

//Pre: Cap
//Post: Cap
void T_LCD_StatusMachine(void);

#endif
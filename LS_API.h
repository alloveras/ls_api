/*
================================================================================
 * Nom: LS API
 * Fitxer: LS_API.c
 * Funcionalitat: Proporciona una API per a l'�s de LS_MAKER
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * �ltima Revisi�: 26/01/2015
================================================================================
*/

#ifndef _LS_API_H_
#define _LS_API_H_

//Includes del MCU
#include<xc.h>


/*
================================================================================
                                    KERNEL
================================================================================
*/

//Pre: Cap
//Post: Inicialitza tots els drivers i TADS que composen la API de l'LS Maker.
void LS_Maker_Init(void);

//Pre: Cap
//Post: Cap
//Informaci� : Aquest procediment �s el que mou totes les m�quines d'estat del
//robot, de manera que s'ha de cridar sempre dins del bucle infinit del vostre
//procediment principal perqu� tot funcioni correctament.
void LS_Maker(void);

/*
================================================================================
                                 TAD TIMERS
================================================================================
*/

//Pre: Cap
//Post: Retorna un valor entre 0 i 127 si s'ha pogut obtenir un dels timers
//virtuals del LS Maker. Si el retorn �s -1 llavors �s que no hi ha cap timer
//virtual lliure.
char LS_Maker_Timers_GetTimer(void);

//Pre: nHandler ha d'estar entre 0 i 127.
//Post: Allibera el timer que s'indica a trav�s del Handler perqu� alg� altre
//el pugui utilitzar en un futur.
void LS_Maker_Timers_FreeTimer(char nHandler);

//Pre: nHandler ha d'estar entre 0 i 127
//Post: Retorna la quantitat de milisegons que han passat des de l'�ltim reset
//del timer identificat amb el handler nHandler.
unsigned int LS_Maker_Timers_GetValue(char nHandler);

//Pre : nHandler ha d'estar entre 0 i 127.
//Post: Reseteja el nombre de milisegons que portava acumulats el timer amb
//handler nHandler.
void LS_Maker_Timers_ResetTimer(char nHandler);

/*
================================================================================
                                TAD HEARTBEAT
================================================================================
*/

//Pre: nPeriod ha de valdre entre 0 i 255
//Post: Fixa el temps que triguen els LEDS a canviar d'intensitat.
void LS_Maker_SetHeartBeatPeriod(unsigned char nPeriod);

/*
================================================================================
                                ACCELER�METRE
================================================================================
*/

//Pre: Cap
//Post: Assigna als punters corresponents els valors (en graus) de la inclinaci�
//de la placa respecte als eixos X, Y i Z.
void LS_Maker_ACC_GetXYZ(char *X,char *Y,char *Z);

/*
================================================================================
                                    TAD LCD
================================================================================
*/

//Pre: S'ha d'haver comprovat pr�viament que hi ha lloc per a un nou car�cter a
//la cua de sortida de l'LCD.
//Post: Encua el car�cter a la cua de sortida de l'LCD per ser pintat per
//pantalla quan sigui possible.
void LS_Maker_LCD_PutChar(char byte);

//Pre: Cap
//Post: Retorna 1 (CERT) si hi ha espai a la cua de sortida de l'LCD per a un
//nou car�cter. En qualsevol altre cas, el valor de retorn ser� 0 (FALS).
unsigned char LS_Maker_LCD_FullQueue();

//Pre: Cap
//Post: Retorna 1 (CERT) si s'ha pogut netejar la pantalla LCD de l'LS Maker. En
//qualsevol altre cas, el valor de retorn ser� 0 (FALS).
unsigned char LS_Maker_LCD_ClearScreen();

//Pre: Cap
//Post: Retorna 1 (CERT) si s'ha pogut posar el cursor a la primera posici� de
//la pantalla LCD. En qualsevol altre cas, el valor de retorn ser� 0 (FALS).
unsigned char LS_Maker_LCD_FirstPosition();

/*
================================================================================
                                    USB
================================================================================
*/

//Pre: S'ha d'haver comprovat pr�viament que la cua de sortida no estigui plena.
//Post: Encua el byte rebut per par�metre a la cua de sortida del bus USB per
//tal d'enviar-lo quan sigui possible.
void LS_Maker_USB_WriteByte(char byte);

//Pre: S'ha d'haver comprovat que hi ha almenys un byte a la cua d'entrada del
//bus USB.
//Post: Retorna el primer byte encuat a la cua d'entrda del bus USB. S'ha de
//tenir en compte que la lectura �s DESTRUCTIVA.
char LS_Maker_USB_ReadByte(void);

//Pre: Cap
//Post: Retorna 1 (CERT) si la cua de sortida del bus USB no t� espai disponible
//per un nou byte a enviar. Altrament, aquesta funci� retornar� 0 (FALS).
unsigned char LS_Maker_USB_OutputQueueFull(void);

//Pre: Cap
//Post: Retorna 1 (CERT) si la cua d'entrada del bus USB te al menys 1 byte
//pendent de ser llegit. En qualsevol altre cas, aquesta funci� retornar� 0
//(FALS).
unsigned char LS_Maker_USB_InputQueueEmpty(void);

/*
================================================================================
                                    UART
================================================================================
*/

//Pre: S'ha d'haver comprovat pr�viament que la cua de sortida no estigui plena.
//Post: Encua el byte rebut per par�metre a la cua de sortida de la UART per
//tal d'enviar-lo quan sigui possible.
void LS_Maker_UART_WriteByte(char byte);

//Pre: S'ha d'haver comprovat que hi ha almenys un byte a la cua d'entrada de la
//UART.
//Post: Retorna el primer byte encuat a la cua d'entrda de la UART. S'ha de
//tenir en compte que la lectura �s DESTRUCTIVA.
char LS_Maker_UART_ReadByte(void);

//Pre: Cap
//Post: Retorna 1 (CERT) si la cua de sortida de la UART no t� espai disponible
//per un nou byte a enviar. Altrament, aquesta funci� retornar� 0 (FALS).
unsigned char LS_Maker_UART_OutputQueueFull(void);

//Pre: Cap
//Post: Retorna 1 (CERT) si la cua d'entrada de la UART te al menys 1 byte
//pendent de ser llegit. En qualsevol altre cas, aquesta funci� retornar� 0
//(FALS).
unsigned char LS_Maker_UART_InputQueueEmpty(void);


#endif


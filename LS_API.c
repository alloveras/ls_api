/*
================================================================================
 * Nom: LS API
 * Fitxer: LS_API.c
 * Funcionalitat: Proporciona una API per a l'�s de LS_MAKER
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * �ltima Revisi�: 11/10/2014
================================================================================
*/

//Include de la cap�alera p�blica de la API
#include "LS_API.h"

//Includes dels drivers dels perif�rics
#include "LEDS.h"
#include "UART.h"
#include "USB.h"
#include "I2C.h"
#include "ACC.h"

//Includes dels TADS
#include "T_HeartBeat.h"
#include "T_Timers.h"
#include "T_LCD.h"
#include "T_Propaganda.h"
#include "T_Motors.h"


// REGISTRES DE CONFIGURACI� DEL PIC - 24.1 del Datasheet
_CONFIG2(		IESO_OFF				// Two Speed Start-up               DISABLED
		 	&	FNOSC_FRCPLL        		// Fast RC oscillator w/ divide and PLL
		 	& 	FCKSM_CSDCMD			// Clock switching and clock monitor
			&	OSCIOFNC_ON			// OSCO/RC15 function               RC15
			&	POSCMOD_NONE			// Primary disabled
);
_CONFIG1(		JTAGEN_OFF                              // JTAG                             DISABLED
			&	GCP_OFF				// Code Protect                     DISABLED
			&	GWRP_OFF			// Write Protect                    DISABLED
			&	BKBUG_OFF			// Background Debugger              DISABLED
			&	COE_OFF				// Clip-on Emulation                DISABLED
			&	ICS_PGx2			// ICD pins select share            PGC1/PGD1
			&	FWDTEN_OFF			// Watchdog Timer                   DISABLED
			&	WINDIS_OFF			// Windowed WDT                     DISABLED
			&	FWPSA_PR128			// Watchdog prescaler	1:128
			&	WDTPS_PS2048			// Watchdog postscale	1:2048      Pre=128 i Post=2048 --> WatchDog Timer = 8seg
);


/*
================================================================================
                            PROCEDIMENTS DEL KERNEL
================================================================================
*/

//Pre : Cap
//Post : Inicialitza l'LS Maker amb les configuracions de drivers i TADS que
//s'han rebut per par�metres
void LS_Maker_Init(){

        //Inicialitzacions v�ries
        CLKDIV=0x0000;          //Desactivem la divisi� del rellotge del MCU
        RCONbits.SWDTEN = 0;    //Desactivem el watch dog timer
        
        T_Timers_Init();    //Carreguem el TAD Timer
        LEDS_Init();        //Inicialitzem els LEDS
        UART_Init();        //Inicialitzem UART
        USB_Init();         //Inicialitzem USB
        I2C1_Init();        //Inicialitzem I2C (Canal 1)
        ACC_Init();         //Inicialitzem Acceler�metre
        T_HeartBeat_Init(); //Inicialitzem HeartBeat
        T_LCD_Init();       //Inicialitzem LCD
        T_PROPAGANDA_Init();//Inicialitzem propaganda
        T_MOTORS_Init();    //Inicialitzem motors
        

}

//Pre: Cap
//Post: Executa un pas de totes les m�quines d'estats que han estat configurades
//tant dels drivers com dels TADS.
void LS_Maker(){

    LEDS_StatusMachine();
    I2C1_StatusMachine();
    T_HeartBeat_StatusMachine();
    T_LCD_StatusMachine();
    I2C1_StatusMachine();
    USB_StatusMachine();
    I2C1_StatusMachine();
    UART_StatusMachine();
    I2C1_StatusMachine();
    ACC_StatusMachine();
    I2C1_StatusMachine();
    T_PROPAGANDA_StatusMachine();
    I2C1_StatusMachine();
    T_MOTORS_StatusMachine();
    I2C1_StatusMachine();


}

/*
================================================================================
                    PROCEDIMENTS I FUNCIONS TAD TIMERS
================================================================================
*/

//Pre: Cap
//Post: Retorna un valor entre 0 i 127 si s'ha pogut obtenir un dels timers
//virtuals del LS Maker. Si el retorn �s -1 llavors �s que no hi ha cap timer
//virtual lliure.
char LS_Maker_Timers_GetTimer(void){
    return T_Timers_GetTimer();
}

//Pre: nHandler ha d'estar entre 0 i 127.
//Post: Allibera el timer que s'indica a trav�s del Handler perqu� alg� altre
//el pugui utilitzar en un futur.
void LS_Maker_Timers_FreeTimer(char nHandler){
    T_Timers_FreeTimer(nHandler);
}

//Pre: nHandler ha d'estar entre 0 i 127
//Post: Retorna la quantitat de milisegons que han passat des de l'�ltim reset
//del timer identificat amb el handler nHandler.
unsigned int LS_Maker_Timers_GetValue(char nHandler){
    return T_Timers_GetValue(nHandler);
}

//Pre : nHandler ha d'estar entre 0 i 127.
//Post: Reseteja el nombre de milisegons que portava acumulats el timer amb
//handler nHandler.
void LS_Maker_Timers_ResetTimer(char nHandler){
    T_Timers_ResetTimer(nHandler);
}

/*
================================================================================
                    PROCEDIMENTS I FUNCIONS TAD HEARTBEAT
================================================================================
*/

//Pre: nPeriod ha de valdre entre 0 i 255
//Post: Fixa el temps que triguen els LEDS a canviar d'intensitat.
void LS_Maker_SetHeartBeatPeriod(unsigned char nPeriod){
    T_HeartBeat_SetPeriod(nPeriod);
}

/*
================================================================================
                    PROCEDIMENTS I FUNCIONS ACCELER�METRE
================================================================================
*/

//Pre: Cap
//Post: Assigna als punters corresponents els valors (en graus) de la inclinaci�
//de la placa respecte als eixos X, Y i Z.
void LS_Maker_ACC_GetXYZ(char *X,char *Y,char *Z){
    ACC_GetXYZ(X,Y,Z);
}

/*
================================================================================
                                   LCD
================================================================================
*/

//Pre: S'ha d'haver comprovat pr�viament que hi ha lloc per a un nou car�cter a
//la cua de sortida de l'LCD.
//Post: Encua el car�cter a la cua de sortida de l'LCD per ser pintat per
//pantalla quan sigui possible.
void LS_Maker_LCD_PutChar(char byte){
   T_LCD_PutChar(byte);
}

//Pre: Cap
//Post: Retorna 1 (CERT) si hi ha espai a la cua de sortida de l'LCD per a un
//nou car�cter. En qualsevol altre cas, el valor de retorn ser� 0 (FALS).
unsigned char LS_Maker_LCD_FullQueue(){
    return T_LCD_FullQueue();
}

//Pre: Cap
//Post: Retorna 1 (CERT) si s'ha pogut netejar la pantalla LCD de l'LS Maker. En
//qualsevol altre cas, el valor de retorn ser� 0 (FALS).
unsigned char LS_Maker_LCD_ClearScreen(){
    return T_LCD_ClearScreen();
}

//Pre: Cap
//Post: Retorna 1 (CERT) si s'ha pogut posar el cursor a la primera posici� de
//la pantalla LCD. En qualsevol altre cas, el valor de retorn ser� 0 (FALS).
unsigned char LS_Maker_LCD_FirstPosition(){
    return T_LCD_FirstPosition();
}

/*
================================================================================
                                    USB
================================================================================
*/

//Pre: S'ha d'haver comprovat pr�viament que la cua de sortida no estigui plena.
//Post: Encua el byte rebut per par�metre a la cua de sortida del bus USB per
//tal d'enviar-lo quan sigui possible.
void LS_Maker_USB_WriteByte(char byte){
    USB_WriteByte(byte);
}

//Pre: S'ha d'haver comprovat que hi ha almenys un byte a la cua d'entrada del
//bus USB.
//Post: Retorna el primer byte encuat a la cua d'entrda del bus USB. S'ha de
//tenir en compte que la lectura �s DESTRUCTIVA.
char LS_Maker_USB_ReadByte(void){
    return USB_ReadByte();
}

//Pre: Cap
//Post: Retorna 1 (CERT) si la cua de sortida del bus USB no t� espai disponible
//per un nou byte a enviar. Altrament, aquesta funci� retornar� 0 (FALS).
unsigned char LS_Maker_USB_OutputQueueFull(void){
    return USB_OutputQueueFull();
}

//Pre: Cap
//Post: Retorna 1 (CERT) si la cua d'entrada del bus USB te al menys 1 byte
//pendent de ser llegit. En qualsevol altre cas, aquesta funci� retornar� 0
//(FALS).
unsigned char LS_Maker_USB_InputQueueEmpty(void){
    return USB_InputQueueEmpty();
}

/*
================================================================================
                                    UART
================================================================================
*/

//Pre: S'ha d'haver comprovat pr�viament que la cua de sortida no estigui plena.
//Post: Encua el byte rebut per par�metre a la cua de sortida de la UART per
//tal d'enviar-lo quan sigui possible.
void LS_Maker_UART_WriteByte(char byte){
    UART_WriteByte(byte);
}

//Pre: S'ha d'haver comprovat que hi ha almenys un byte a la cua d'entrada de la
//UART.
//Post: Retorna el primer byte encuat a la cua d'entrda de la UART. S'ha de
//tenir en compte que la lectura �s DESTRUCTIVA.
char LS_Maker_UART_ReadByte(void){
    return UART_ReadByte();
}

//Pre: Cap
//Post: Retorna 1 (CERT) si la cua de sortida de la UART no t� espai disponible
//per un nou byte a enviar. Altrament, aquesta funci� retornar� 0 (FALS).
unsigned char LS_Maker_UART_OutputQueueFull(void){
    return UART_OutputQueueFull();
}

//Pre: Cap
//Post: Retorna 1 (CERT) si la cua d'entrada de la UART te al menys 1 byte
//pendent de ser llegit. En qualsevol altre cas, aquesta funci� retornar� 0
//(FALS).
unsigned char LS_Maker_UART_InputQueueEmpty(void){
    return UART_InputQueueEmpty();
}

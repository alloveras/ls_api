/*
================================================================================
 * Nom: TAD MOTORS
 * Fitxer: T_Motors.c
 * Funcionalitat: Permet la variaci� de la velocitat dels motors del tricopter
 * mitjan�ant la comunicaci� amb el MCU dedicat als PWM a trav�s del protocol
 * propi de LS_COPTER.
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * �ltima Revisi�: 29/01/2015
================================================================================
*/

#include "T_Motors.h"

/*
 ===============================================================================
                      PROTOCOL DE COMUNICACI� I TRAMA
 ===============================================================================
 */

#define STOP_ENGINES 0x00
#define CHANGE_SPEED 0x01
#define ACK 0x02

#define START "\fPrem ENTER per continuar!\0"
#define INFO "\fCONTROL REMOT LS_COPTER v1.0\n\rOpcions a triar:\n\r\0"
#define MOTOR1 "1 - Motor 1 ++ \n\r2 - Motor 1 --\n\r\0"
#define MOTOR2 "3 - Motor 2 ++ \n\r4 - Motor 2 --\n\r\0"
#define MOTOR3 "5 - Motor 3 ++ \n\r6 - Motor 3 --\n\r\0"
#define STOP "7 - Parar motors\n\r\0"
#define OPCIO "Opcio: \0"
#define OK "\n\r\n\rPeticio Processada!\0"
#define KO "\n\r\n\rError al processar la peticio!\0"
#define NO_EXISTEIX "\n\r\n\rNo existeix aquesta opcio\0"

static unsigned char nEstat = 0;
static unsigned char nTimer = -1;
static unsigned char nCounter = 0;
static unsigned char nSpeed1 = 0;
static unsigned char nSpeed2 = 0;
static unsigned char nSpeed3 = 0;
static unsigned char nSpeed1Temp = 0;
static unsigned char nSpeed2Temp = 0;
static unsigned char nSpeed3Temp = 0;
static char cRead = ' ';


/*
 ===============================================================================
                         FUNCIONS I PROCEDIMENTS P�BLICS
 ===============================================================================
 */

//Pre: El TAD Timers ha d'estar inicialitzat
//Post: Demana un timer i inicialitza les cues del TAD Motors.
void T_MOTORS_Init(void){

    //Demanem un timer per utilizar-lo pels timeouts
    nTimer = T_Timers_GetTimer();

    nCounter = 0;
    nEstat = 0;
    nSpeed1 = 0;
    nSpeed2 = 0;
    nSpeed3 = 0;

}



//Pre: Cap
//Post: Cap
void T_MOTORS_StatusMachine(void){
    
    
    switch(nEstat){

        case 0:
            if(START[nCounter] != '\0'){
                if(USB_OutputQueueFull() == 0){
                    USB_WriteByte(START[nCounter++]);
                }
            }else{
                nEstat++;
                nCounter = 0;
                T_Timers_ResetTimer(nTimer);
            }
            break;

        case 1:
            if(USB_InputQueueEmpty() == 0){
                if(USB_ReadByte() == '\r'){
                    nCounter = 0;
                    nEstat++;
                }
            }else{
                if(T_Timers_GetValue(nTimer) >= 100){
                    nEstat = 0;
                }
            }
            break;

        case 2:
            if(INFO[nCounter] != '\0'){
                if(USB_OutputQueueFull() == 0){
                    USB_WriteByte(INFO[nCounter++]);
                }
            }else{
                nCounter = 0;
                nEstat++;
            }
            break;
        case 3:
            if(MOTOR1[nCounter] != '\0'){
                if(USB_OutputQueueFull() == 0){
                    USB_WriteByte(MOTOR1[nCounter++]);
                }
            }else{
                nCounter = 0;
                nEstat++;
            }
            break;
        case 4:
            if(MOTOR2[nCounter] != '\0'){
                if(USB_OutputQueueFull() == 0){
                    USB_WriteByte(MOTOR2[nCounter++]);
                }
            }else{
                nEstat++;
                nCounter = 0;
            }
            break;
        case 5:
            if(MOTOR3[nCounter] != '\0'){
                if(USB_OutputQueueFull() == 0){
                    USB_WriteByte(MOTOR3[nCounter++]);
                }
            }else{
                nCounter = 0;
                nEstat++;
            }
            break;
        case 6:
            if(STOP[nCounter] != '\0'){
                if(USB_OutputQueueFull() ==  0){
                    USB_WriteByte(STOP[nCounter++]);
                }
            }else{
                nCounter = 0;
                nEstat++;
            }
            break;
        case 7:
            if(OPCIO[nCounter] != '\0'){
                if(USB_OutputQueueFull() == 0){
                    USB_WriteByte(OPCIO[nCounter++]);
                }
            }else{
                nCounter = 0;
                nEstat++;
            }
            break;

        case 8:
            //Esperem que ens premim una tecla
            if(USB_InputQueueEmpty() == 0){
                cRead = USB_ReadByte();
                nEstat++;
            }
            break;

        case 9:
            //En funci� de la tecla enviem petici� a la UART
            switch(cRead){

                case '1':
                    nSpeed1Temp++;
                    if(UART_OutputQueueFull() == 0){
                        UART_WriteByte(CHANGE_SPEED);
                        UART_WriteByte(0);
                        UART_WriteByte(nSpeed1Temp);
                        nEstat++;
                        T_Timers_ResetTimer(nTimer);
                    }
                    break;

                case '2':
                    nSpeed1Temp--;
                    if(UART_OutputQueueFull() == 0){
                        UART_WriteByte(CHANGE_SPEED);
                        UART_WriteByte(0);
                        UART_WriteByte(nSpeed1Temp);
                        nEstat++;
                        T_Timers_ResetTimer(nTimer);
                    }
                    break;

                case '3':
                    nSpeed2Temp++;
                    if(UART_OutputQueueFull() == 0){
                        UART_WriteByte(CHANGE_SPEED);
                        UART_WriteByte(1);
                        UART_WriteByte(nSpeed2Temp);
                        nEstat++;
                        T_Timers_ResetTimer(nTimer);
                    }
                    break;

                case '4':
                    nSpeed2Temp--;
                    if(UART_OutputQueueFull() == 0){
                        UART_WriteByte(CHANGE_SPEED);
                        UART_WriteByte(1);
                        UART_WriteByte(nSpeed2Temp);
                        nEstat++;
                        T_Timers_ResetTimer(nTimer);
                    }
                    break;
                case '5':
                    nSpeed3Temp++;
                    if(UART_OutputQueueFull() == 0){
                        UART_WriteByte(CHANGE_SPEED);
                        UART_WriteByte(2);
                        UART_WriteByte(nSpeed3Temp);
                        nEstat++;
                        T_Timers_ResetTimer(nTimer);
                    }
                    break;
                    
                 case '6':
                    nSpeed3Temp--;
                    if(UART_OutputQueueFull() == 0){
                        UART_WriteByte(CHANGE_SPEED);
                        UART_WriteByte(2);
                        UART_WriteByte(nSpeed3Temp);
                        nEstat++;
                        T_Timers_ResetTimer(nTimer);
                    }
                    break;

                case '7':
                    if(UART_OutputQueueFull() == 0){
                        UART_WriteByte(STOP_ENGINES);
                        nEstat++;
                        T_Timers_ResetTimer(nTimer);
                    }
                    break;

                default:
                    nCounter = 0;
                    nEstat = 13;
                    break;
            }
            break;

        case 10:
            if(UART_InputQueueEmpty() == 0){
                if(UART_ReadByte() == ACK){
                    //ANEM A PETICIO OK
                    nEstat = 1;
                    nCounter = 0;
                    T_Timers_ResetTimer(nTimer);
                    nSpeed1 = nSpeed1Temp;
                    nSpeed2 = nSpeed2Temp;
                    nSpeed3 = nSpeed3Temp;
                }
            }else{
                if(T_Timers_GetValue(nTimer) >= 5000){
                    //ANEM A PETICIO ERROR
                    nCounter = 0;
                    nEstat = 12;
                }
            }
            break;


        case 11:
            if(KO[nCounter] != '\0'){
                if(USB_OutputQueueFull() == 0){
                    USB_WriteByte(KO[nCounter++]);
                }
            }else{
                nCounter = 0;
                T_Timers_ResetTimer(nTimer);
                nEstat++;
            }
            break;

        case 12:
            if(T_Timers_GetValue(nTimer) >= 5000){
                nCounter = 0;
                nEstat = 1;
                T_Timers_ResetTimer(nTimer);
            }
            break;

        case 13:
            if(NO_EXISTEIX[nCounter] != '\0'){
                if(USB_OutputQueueFull() == 0){
                    USB_WriteByte(NO_EXISTEIX[nCounter++]);
                }
            }else{
                nCounter = 0;
                T_Timers_ResetTimer(nTimer);
                nEstat++;
            }
            break;
            
        case 14:
            if(T_Timers_GetValue(nTimer) >= 5000){
                nCounter = 0;
                T_Timers_ResetTimer(nTimer);
                nEstat = 1;
            }

            break;


    }

        
}



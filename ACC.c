/*
================================================================================
 * Nom: DRIVER ACCELER�METRE
 * Fitxer: UART.c
 * Funcionalitat: Permet llegir les dades de l'acceler�metre del robot a trav�s
 * del bus I2C.
 * Autor: Albert Lloveras Carbonell (alloveras@salleurl.edu)
 * �ltima Revisi�: 26/01/2015
================================================================================
*/

//Include del fitxer de cap�aleres
#include "ACC.h"

/*
================================================================================
                            CONSTANTS PRIVADES
================================================================================
*/

#define ACC_ADDR 0x4C       //Adre�a I2C de l'acceler�metre
#define R_MODE 0x07         //Registre de configuraci� del mode
#define R_INTSU 0x06        //Registre de configuraci� d'interrupcions
#define R_SAMPLERATE 0x08   //Registre de configuraci� del mostreig
#define R_XOUT 0x00         //Registre d'inclinaci� eix X
#define R_YOUT 0x01         //Registre d'inclinaci� eix Y
#define R_ZOUT 0x02         //Registre d'inclinaci� eix Z

#define MAX_PROMIG 4        //Nombre de lectures sobre la que promitjarem
#define ALERT (1 << 6)      //M�scara pel bit d'alerta de l'acceler�metre
#define BIT5 (1 << 5)       //M�scara pel bit de signe de l'acceler�metre

/*
================================================================================
                               VARIABLES PRIVADES
================================================================================
*/

static unsigned char nEstat = 0;    //Indicador d'estat de la m�quina d'estats
static unsigned char nTimer = -1;   //Handler del timer
static unsigned char nCounter = 0;  //Comptador de mostres
static char nXPos = 0;              //Inclinaci� eix X actual
static char nYPos = 0;              //Inclinaci� eix Y actual
static char nZPos = 0;              //Inclinaci� eix Z actual
static char nXPromig = 0;           //Auxiliar utilitzat per promitjar les X
static char nYPromig = 0;           //Auxiliar utilitzat per promitjar les Y
static char nZPromig = 0;           //Auxiliar utilitzat per promitjar les Z
static char nZTemp = 0;             //Temporal per llegir les X de l'ACC
static char nYTemp = 0;             //Temporal per llegir les Y de l'ACC
static char nXTemp = 0;             //Temporal per llegir les Z de l'ACC

/*
================================================================================
                        FUNCIONS I PROCEDIMENTS P�BLICS
================================================================================
*/

//Pre: El TAD Timers ha d'estar inicialitzat
//Post: Activa l'alimentaci� de l'acceler�metre a trav�s del transistor de
//control i fixa les configuracions necess�ries per obtenir orientaci� en els
//eixos X, Y i Z.
void ACC_Init(void){

    nTimer = T_Timers_GetTimer();

    if(nTimer != -1){

        //Donem tensi� a l'Acceler�metre
        TRISDbits.TRISD8 = 0;
        LATDbits.LATD8 = 0;

        //Posem l'acceler�metre en mode rep�s
        I2C1_WriteByte(ACC_ADDR,R_MODE, 0x00);
        //Activem Tap i Shake
	I2C1_WriteByte(ACC_ADDR, R_INTSU,0xE4);
	// I Poso el m�xim de mostres per segon (120)
	I2C1_WriteByte(ACC_ADDR,R_SAMPLERATE, 0x00);

        //Activem l'Acceler�metre de nou
        I2C1_WriteByte(ACC_ADDR,R_MODE,0x01);

        //Resetegem estat i timer
        nEstat = 0;
        T_Timers_ResetTimer(nTimer); 

    }

}

//Pre: Els par�metres X, Y i Z han de ser punters a variables del tipus CHAR i
//MAI podr�n valdre NULL.
//Post: Assigna la quatitat de graus d'inclinaci� entre la placa del robot i
//els eixos de coordenades.
void ACC_GetXYZ(char *X, char *Y, char *Z){
    (*X) = nXPos;
    (*Y) = nYPos;
    (*Z) = nZPos;
}

//Pre: Cap
//Post: Cap
void ACC_StatusMachine(void){

    switch(nEstat){
        case 0:
            //Ens esperem 100 ms abans no fem una nova lectura
            if(T_Timers_GetValue(nTimer) >= 300){
                nXPromig = 0;
                nYPromig = 0;
                nZPromig = 0;
                nEstat++;
            }
            break;
        case 1:
            if(I2C1_isReceiving() == 0){
                I2C1_ReadByte(ACC_ADDR,R_XOUT,&nXTemp);
                nEstat++;
            }
            break;
        case 2:
            if(I2C1_isReceiving() == 0){
                if((nXTemp & ALERT) == 0){
                    if((nXTemp & BIT5)){
                        nXPromig +=  ((nXTemp & 0x1F) - 32);
                    }else{
                        nXPromig += (nXTemp & 0x1F);
                    }
                    nEstat++;
                }else{
                    nEstat = 1;
                }
            }
            break;
        case 3:
            if(I2C1_isReceiving() == 0){
                I2C1_ReadByte(ACC_ADDR,R_YOUT,&nYTemp);
                nEstat++;
            }
            break;
        case 4:
            if(I2C1_isReceiving() == 0){
                if((nYTemp & ALERT) == 0){
                    if(nYTemp & BIT5){
                        nYPromig += ((nYTemp & 0x1F) - 32);
                    }else{
                        nYPromig += (nYTemp & 0x1F);
                    }
                    nEstat++;
                }else{
                    nEstat = 3;
                }
            }
            break;
        case 5:
             if(I2C1_isReceiving() == 0){
                I2C1_ReadByte(ACC_ADDR,R_ZOUT,&nZTemp);
                nEstat++;
            }
            break;
        case 6:
            if(I2C1_isReceiving() == 0){
                if((nZTemp & ALERT) == 0){
                    if((nZTemp & BIT5)){
                        nZPromig += ((nZTemp & 0x1F) - 32);
                    }else{
                        nZPromig += (nZTemp & 0x1F);
                    }
                    nEstat++;
                    nCounter++;
                }else{
                    nEstat = 5;
                }
            }
            break;
        case 7:
            if(nCounter >= MAX_PROMIG){
                nEstat++;
            }else{
                nEstat = 1;
            }
            break;
        case 8:
            nCounter = 0;

            //Realitzem el promig
            nXPos = nXPromig / MAX_PROMIG;
            nYPos = nYPromig / MAX_PROMIG;
            nZPos = nZPromig / MAX_PROMIG;

            //Estandaritzem mesures en angles entre 0 i 90 �
            nXPos = (4.285714 * nXPos) - 5;
            if(nXPos > 90) nXPos = 90;
            if(nXPos < -90) nXPos = -90;

            //Estandaritzem mesures en angles entre 0 i 90 �
            nYPos = (4.285714 * nYPos);
            if(nYPos > 90) nYPos = 90;
            if(nYPos < -90) nYPos = -90;

            //Estandaritzem les mesures en angles entre 0 i 90
            if(nZPos >= 0){
                nZPos = (-3.81) + 90;
                if(nZPos > 90) nZPos = 90;
            }else{
                nZPos = (-3.81) - 90;
                if(nZPos < -90) nZPos = -90;
            }

            //Resetegem Timer i tornem a l'estat inicial
            T_Timers_ResetTimer(nTimer);
            nEstat = 0;
            
            break;

    }

}